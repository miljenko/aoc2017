with open('06_input.txt') as f:
    inp = [int(i) for i in f.read().split()]

seen = {tuple(inp)}
cycles = 0
n = len(inp)

while True:
    cycles += 1
    idx_max, val_max = max(enumerate(inp), key=lambda k: k[1])
    each_gets, remaining = divmod(val_max, n)
    for i in range(n):
        inp[i] += each_gets
    inp[idx_max] = each_gets

    j = idx_max + 1
    while remaining > 0:
        if j == n:
            j = 0
        inp[j] += 1
        remaining -= 1
        j += 1

    tinp = tuple(inp)
    if tinp in seen:
        break
    seen.add(tinp)

print(cycles)
