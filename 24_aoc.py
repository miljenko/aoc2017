def find(val_needed, sum_so_far, available_ports):
    # print('find', val_needed, sum_so_far, available_ports)
    max_sum = sum_so_far
    for n, next_avaiable in neighbors(val_needed, available_ports):
        new_sum = sum_so_far + n[0] + n[1]
        s = find(n[0] if val_needed == n[1] else n[1], new_sum, next_avaiable)
        if s > max_sum:
            max_sum = s
    return max_sum

def neighbors(val_needed, available_ports):
    for p in available_ports:
        if val_needed in p:
            next_avaiable = available_ports - {p}
            yield p, next_avaiable

with open('24_input.txt') as f:
    ports = set(tuple(int(p) for p in line.split('/')) for line in f)

strength = find(0, 0, ports)
print(strength)
