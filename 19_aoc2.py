with open('19_input.txt') as f:
    diagram = [line.strip('\n') for line in f]

def at(p):
    if (p[0] < 0 or p[0] >= len(diagram)
            or p[1] < 0 or p[1] >= len(diagram[p[0]])):
        return ' '
    return diagram[p[0]][p[1]]

pos = (0, diagram[0].index('|'))
dr, dc = 1, 0
path = []
count = 0
while True:
    ch = at(pos)
    # print(pos, ch, dr, dc)
    if ch == '+': # must turn R or L
        if dr == 0:
            dr, dc = 1, 0
        else:
            dr, dc = 0, 1

        if at((pos[0] + dr, pos[1] + dc)) == ' ':
            dr *= -1
            dc *= -1
    elif ch == ' ':
        break
    elif ch not in '|-':
        path.append(ch)

    pos = (pos[0] + dr, pos[1] + dc)
    count += 1

print(count)
