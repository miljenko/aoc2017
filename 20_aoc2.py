import re

def update(p):
    p['v'] = [pv + pa for pv, pa in zip(p['v'], p['a'])]
    p['p'] = [pp + pv for pp, pv in zip(p['p'], p['v'])]
    return p

def distance(p):
    return sum(abs(v) for v in p['p'])

buffer = []
with open('20_input.txt') as f:
    for line in f:
        vals = [int(i) for i in re.findall(r'-?\d+', line)]
        d = dict(zip('pva', (vals[:3], vals[3:6], vals[6:])))
        buffer.append(d)

while True:
    buffer = [update(p) for p in buffer]
    seen, to_remove = set(), set()
    for p in buffer:
        tp = tuple(p['p'])
        if tp not in seen:
            seen.add(tp)
        else:
            to_remove.add(tp)
    buffer = [p for p in buffer if tuple(p['p']) not in to_remove]
    print(len(buffer))
