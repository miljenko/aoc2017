with open('06_input.txt') as f:
    inp = [int(i) for i in f.read().split()]

seen = {tuple(inp)}
loop_size = 0
loop_started = False
loop_start = None
n = len(inp)

while True:
    idx_max, val_max = max(enumerate(inp), key=lambda k: k[1])
    each_gets, remaining = divmod(val_max, n)
    for i in range(n):
        inp[i] += each_gets
    inp[idx_max] = each_gets

    j = idx_max + 1
    while remaining > 0:
        if j == n:
            j = 0
        inp[j] += 1
        remaining -= 1
        j += 1

    tinp = tuple(inp)

    if not loop_started:
        if tinp in seen:
            loop_started = True
            loop_start = tinp
        else:
            seen.add(tinp)
    else:
        loop_size += 1
        if tinp == loop_start:
            break

print(loop_size)
