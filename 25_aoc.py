from collections import defaultdict

def run(cursor, state):
    val = tape[cursor]
    v, d, s = tm[state][val]
    tape[cursor] = v
    return cursor+d, s

tm = {
    'A': [(1, 1, 'B'), (0, -1, 'C')],
    'B': [(1, -1, 'A'), (1, 1, 'D')],
    'C': [(0, -1, 'B'), (0, -1, 'E')],
    'D': [(1, 1, 'A'), (0, 1, 'B')],
    'E': [(1, -1, 'F'), (1, -1, 'C')],
    'F': [(1, 1, 'D'), (1, 1, 'A')]
}

N = 12667664
tape = defaultdict(int)
c, st = 0, 'A'

for _ in range(N):
    c, st = run(c, st)

print(list(tape.values()).count(1))
