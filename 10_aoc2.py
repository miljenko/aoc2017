from functools import reduce
from operator import xor

with open('10_input.txt') as f:
    inp = [ord(c) for c in f.read().strip()]
    inp.extend((int(i) for i in '17, 31, 73, 47, 23'.split(',')))

def knot_hash(l, pos, skip):
    for i in inp:
        if pos+i >= n:
            over = pos + i -n
            rev = (l[pos:] + l[0:over])[::-1]
            l[pos:] = rev[:i-over]
            l[:over] = rev[i-over:]
        else:
            l[pos:pos+i] = reversed(l[pos:pos+i])

        pos += i + skip
        pos %= n
        skip += 1

    return l, pos, skip

n = 256
l = list(range(n))
pos = 0
skip = 0

for j in range(64):
    l, pos, skip = knot_hash(l, pos, skip)

dense_hash = [reduce(xor, l[k*16:(k+1)*16]) for k in range(n//16)]
final_hash = ''.join('%02x' % d for d in dense_hash)

print(final_hash)
