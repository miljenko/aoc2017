def cube_distance(a, b):
    return (abs(a[0] - b[0]) + abs(a[1] - b[1]) + abs(a[2] - b[2])) // 2

with open('11_input.txt') as f:
    inp = [m for m in f.read().strip().split(',')]

start = 0, 0, 0
pos = start

dirs = {
    'n': (0, 1, -1),
    'ne': (1, 0, -1),
    'se': (1, -1, 0),
    's': (0, -1, 1),
    'sw': (-1, 0, 1),
    'nw': (-1, 1, 0)
}

for d in inp:
    step = dirs[d]
    pos = pos[0]+step[0], pos[1]+step[1], pos[2]+step[2]

print(cube_distance(start, pos))
