import re
from collections import defaultdict

line_re = re.compile(r'([a-z]+) \((\d+)\)( -> ([a-z]+(, [a-z]+)*))?')
tower = {}

with open('07_input.txt') as f:
    for line in f:
        g = line_re.match(line).groups()
        p, w, subp = g[0], int(g[1]), g[3].split(', ') if g[3] else []

        if p not in tower:
            tower[p] = {'parent': None}

        tower[p]['subp'] = subp
        tower[p]['w'] = w
        tower[p]['subw'] = 0
        tower[p]['done'] = False

        for s in subp:
            if s in tower:
                tower[s]['parent'] = p
            else:
                tower[s] = {'parent': p}

for p in tower:
    if not tower[p]['parent']:
        bottom = p
        break

# calculate subweights
cur_level = {p for p in tower if not tower[p]['subp']} # start = nodes with no children
while cur_level:
    for c in cur_level:
        parent = tower[c]['parent']
        if parent is not None:
            tower[parent]['subw'] += tower[c]['w'] + tower[c]['subw']
        tower[c]['done'] = True

    next_level = {p for p in tower # unprocessed nodes with all children processed
                  if not tower[p]['done']
                  and all(tower[s]['done'] for s in tower[p]['subp'])}

    cur_level = next_level

# find the unbalanced one
unbalanced_id = bottom
while True:
    # print(unbalanced_id)
    c = defaultdict(list)
    for s in tower[unbalanced_id]['subp']:
        c[tower[s]['w'] + tower[s]['subw']] += [s]
    single = [v[0] for k, v in c.items() if len(v) == 1]
    if not single:
        break
    unbalanced_id = single[0]

# correct weight
unbalanced = tower[unbalanced_id]
siblings = tower[unbalanced['parent']]['subp'][:]
siblings.remove(unbalanced_id)
balanced = tower[siblings[0]]
diff = unbalanced['w'] + unbalanced['subw'] - balanced['w'] - balanced['subw']
print(unbalanced['w'] - diff)
