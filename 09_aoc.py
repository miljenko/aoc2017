def score(line):
    line_score = 0
    open_groups = 0
    in_garbage = False
    skip = False
    for c in line:
        if skip:
            skip = False
            continue
        if c == '{' and not in_garbage:
            open_groups += 1
        elif c == '}' and not in_garbage:
            line_score += open_groups
            open_groups -= 1
        elif c == '!':
            skip = True
        elif c == '<':
            in_garbage = True
        elif c == '>':
            in_garbage = False

    # print(line, line_score)
    return line_score

with open('09_input.txt') as f:
    print(sum(score(line.strip()) for line in f))
