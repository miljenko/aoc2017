with open('01_input.txt') as f:
    inp = f.read().strip()

half = len(inp) // 2
ans = sum(int(i) for i, j in zip(inp, inp[half:]+inp[:half]) if i == j)
print(ans)
