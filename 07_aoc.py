import re

line_re = re.compile(r'([a-z]+) \((\d+)\)( -> ([a-z]+(, [a-z]+)*))?')
tower = {}

with open('07_input.txt') as f:
    for line in f:
        g = line_re.match(line).groups()
        p, w, subp = g[0], int(g[1]), g[3].split(', ') if g[3] else []

        if p not in tower:
            tower[p] = {'parent': None}

        tower[p]['subp'] = subp

        for s in subp:
            if s in tower:
                tower[s]['parent'] = p
            else:
                tower[s] = {'parent': p}

for p in tower:
    if not tower[p]['parent']:
        print(p)
