from utils import knot_hash

inp = 'hfdlxzhv'
used = 0

for i in range(128):
    s = f'{inp}-{i}'
    kh = knot_hash(s)
    bin_kh = ''.join('{:04b}'.format(int(c, 16)) for c in kh)
    used += bin_kh.count('1')

print(used)
