with open('02_input.txt') as f:
    inp = [[int(i) for i in line.split()] for line in f]

def divide(row):
    for i, n1 in enumerate(row):
        for n2 in row[i+1:]:
            if n1 % n2 == 0:
                return n1//n2
            elif n2 % n1 == 0:
                return n2//n1

checksum = sum(divide(row) for row in inp)
print(checksum)
