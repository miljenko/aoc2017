def score(line):
    garbage_count = 0
    in_garbage = False
    skip = False
    for c in line:
        if skip:
            skip = False
            continue
        if c == '!':
            skip = True
        elif c == '<' and not in_garbage:
            in_garbage = True
        elif c == '>':
            in_garbage = False
        else:
            if in_garbage:
                garbage_count += 1

    # print(line, garbage_count)
    return garbage_count

with open('09_input.txt') as f:
    print(sum(score(line.strip()) for line in f))
