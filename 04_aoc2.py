with open('04_input.txt') as f:
    inp = [line.split() for line in f]

ans = sum(all(sorted(word) != sorted(word2)
              for j, word in enumerate(words)
              for word2 in words[j+1:])
          for words in inp)

print(ans)
