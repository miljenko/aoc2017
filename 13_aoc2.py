inp = {}

with open('13_input.txt') as f:
    for line in f:
        s = line.strip().split(': ')
        inp[int(s[0])] = int(s[1])

n = max(inp.keys()) + 1
layers = [inp.get(i, 0) for i in range(n)]

ps = 0
while any((ps+i) % (2*(d-1)) == 0 for i, d in enumerate(layers) if d != 0):
    ps += 1

print(ps)
