from itertools import tee

def pairwise(iterable):
    a, b = tee(iterable)
    next(b, None)
    return zip(a, b)

with open('01_input.txt') as f:
    inp = f.read().strip()

ans = sum(int(i) for i, j in pairwise(inp + inp[0]) if i == j)
print(ans)
