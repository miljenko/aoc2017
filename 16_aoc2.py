from string import ascii_lowercase

P = 16
start = programs = ascii_lowercase[:P]

with open('16_input.txt') as f:
    inp = f.read().strip().split(',')

def dance(s):
    l = list(s)
    for m in inp:
        move = m[0]
        if move == 's':
            n = int(m[1:])
            l = l[-n:] + l[:P-n]
        elif move == 'x':
            p1, p2 = (int(i) for i in m[1:].split('/'))
            l[p1], l[p2] = l[p2], l[p1]
        elif move == 'p':
            p1, p2 = m[1:].split('/')
            for i in range(P):
                if l[i] == p1:
                    l[i] = p2
                elif l[i] == p2:
                    l[i] = p1

    return ''.join(l)

rounds = 1000000000
period = 0
for i in range(rounds):
    programs = dance(programs)
    if programs == start:
        period = i+1
        break
else:
    print(programs)

if period:
    r = rounds % period
    for _ in range(r):
        programs = dance(programs)
    print(programs)
