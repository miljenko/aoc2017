inp = 265149

def fill(row, col):
    val = sum(grid.get((rr, cc), 0)
              for rr in range(row-1, row+2)
              for cc in range(col-1, col+2)
              if cc != col or rr != row)
    # print(f'({row}, {col}) = {val}')
    grid[row, col] = val
    return val

def spiral():
    ''' 1R, 1U, 2L, 2D, 3R, 3U, 4L, 4D, ... '''
    row, col = 0, 0
    step = 1
    while True:
        for col in range(col+1, col+step+1): # right
            yield row, col
        for row in range(row-1, row-step-1, -1): # up
            yield row, col
        step += 1

        for col in range(col-1, col-step-1, -1): # left
            yield row, col
        for row in range(row+1, row+step+1): # down
            yield row, col
        step += 1

grid = {(0, 0): 1}

for r, c in spiral():
    val = fill(r, c)
    if val > inp:
        print(val)
        break
