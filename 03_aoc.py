inp = 265149

def spiral():
    ''' 1R, 1U, 2L, 2D, 3R, 3U, 4L, 4D, ... '''
    row, col = 0, 0
    yield row, col
    step = 1
    while True:
        for col in range(col+1, col+step+1): # right
            yield row, col
        for row in range(row-1, row-step-1, -1): # up
            yield row, col
        step += 1

        for col in range(col-1, col-step-1, -1): # left
            yield row, col
        for row in range(row+1, row+step+1): # down
            yield row, col
        step += 1

for i, (r, c) in enumerate(spiral(), start=1):
    if i == inp:
        print(abs(r) + abs(c))
        break
