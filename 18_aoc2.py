from collections import defaultdict, deque
from string import ascii_lowercase

program = []
queue1, queue2 = deque(), deque()
registers1, registers2 = defaultdict(int), defaultdict(int)
registers2['p'] = 1

with open('18_input.txt') as f:
    for line in f:
        instr, params = line[:3], [p if p in ascii_lowercase else int(p)
                                   for p in line[3:].strip().split()]
        program.append((instr, params))

def val(x, registers):
    return x if isinstance(x, int) else registers[x]

def run(lineno, registers, my_queue, other_queue):
    sent = 0
    while lineno < len(program):
        instr, params = program[lineno]
        # print(instr, params, registers)

        if instr == 'snd':
            other_queue.append(val(params[0], registers))
            sent += 1
        elif instr == 'set':
            registers[params[0]] = val(params[1], registers)
        elif instr == 'add':
            registers[params[0]] += val(params[1], registers)
        elif instr == 'mul':
            registers[params[0]] *= val(params[1], registers)
        elif instr == 'mod':
            registers[params[0]] %= val(params[1], registers)
        elif instr == 'rcv':
            if my_queue:
                registers[params[0]] = my_queue.popleft()
            else:
                return lineno, sent
        elif instr == 'jgz':
            if val(params[0], registers) > 0:
                lineno += val(params[1], registers)
                continue

        lineno += 1

    return lineno, sent

lineno1, lineno2 = 0, 0
count = 0
while True:
    prev_queue1, prev_queue2 = list(queue1), list(queue2)
    lineno1, _ = run(lineno1, registers1, queue1, queue2)
    lineno2, sent2 = run(lineno2, registers2, queue2, queue1)
    count += sent2
    if prev_queue1 == list(queue1) and prev_queue2 == list(queue2):
        break

print(count)
