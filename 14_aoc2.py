from collections import deque
from utils import knot_hash

N = 128

def neighbours(r, c):
    if c < N-1: yield r, c+1
    if r < N-1: yield r+1, c
    if c > 0: yield r, c-1
    if r > 0: yield r-1, c

def walk(r, c):
    visited.add((r, c))
    q = deque(neighbours(r, c))
    while q:
        nr, nc = q.popleft()
        if (nr, nc) not in visited and grid[nr][nc] == '1':
            visited.add((nr, nc))
            q.extend(neighbours(nr, nc))

inp = 'hfdlxzhv'
grid = []
visited = set()

for i in range(N):
    s = f'{inp}-{i}'
    kh = knot_hash(s)
    bin_kh = ''.join('{:04b}'.format(int(c, 16)) for c in kh)
    grid.append(bin_kh)

groups = 0
for row in range(N):
    for col in range(N):
        if (row, col) not in visited and grid[row][col] == '1':
            walk(row, col)
            groups += 1

print(groups)
