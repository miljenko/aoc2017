with open('02_input.txt') as f:
    inp = [[int(i) for i in line.split()] for line in f]

checksum = sum(max(row) - min(row) for row in inp)
print(checksum)
