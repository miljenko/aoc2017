import re

def update(p):
    p['v'] = [pv + pa for pv, pa in zip(p['v'], p['a'])]
    p['p'] = [pp + pv for pp, pv in zip(p['p'], p['v'])]
    return p

def distance(p):
    return sum(abs(v) for v in p['p'])

buffer = []
with open('20_input.txt') as f:
    for line in f:
        vals = [int(i) for i in re.findall(r'-?\d+', line)]
        d = dict(zip('pva', (vals[:3], vals[3:6], vals[6:])))
        buffer.append(d)

while True:
    pn, m = min(enumerate(distance(p) for p in buffer), key=lambda k: k[1])
    print(pn)#, m)
    buffer = [update(p) for p in buffer]
