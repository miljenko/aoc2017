with open('04_input.txt') as f:
    inp = [line.split() for line in f]

ans = sum(len(words) == len(set(words)) for words in inp)
print(ans)
