with open('15_input.txt') as f:
    a, b = (int(line.split()[-1]) for line in f)

factor_a = 16807
factor_b = 48271
div = 2147483647
n = 40000000
count = 0

def bin_low16(i):
    return '{:016b}'.format(i)[-16:]

for i in range(n):
    a = a * factor_a % div
    b = b * factor_b % div

    if bin_low16(a) == bin_low16(b):
        count += 1

print(count)
