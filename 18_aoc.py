from collections import defaultdict
from string import ascii_lowercase

program = []
registers = defaultdict(int)
snd = 0

with open('18_input.txt') as f:
    for line in f:
        instr, params = line[:3], [p if p in ascii_lowercase else int(p)
                                   for p in line[3:].strip().split()]
        program.append((instr, params))

def val(x):
    return x if isinstance(x, int) else registers[x]

lineno = 0
while lineno < len(program):
    instr, params = program[lineno]
    # print(instr, params, registers)

    if instr == 'snd':
        snd = val(params[0])
    elif instr == 'set':
        registers[params[0]] = val(params[1])
    elif instr == 'add':
        registers[params[0]] += val(params[1])
    elif instr == 'mul':
        registers[params[0]] *= val(params[1])
    elif instr == 'mod':
        registers[params[0]] %= val(params[1])
    elif instr == 'rcv':
        if val(params[0]) > 0:
            print(snd)
            break
    elif instr == 'jgz':
        if val(params[0]) > 0:
            lineno += val(params[1])
            continue

    lineno += 1
