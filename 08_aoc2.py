from collections import defaultdict
from operator import lt, le, eq, ne, ge, gt

program = []
registers = defaultdict(int)
high = 0

ops = {
    '<' : lt,
    '<=': le,
    '==': eq,
    '!=': ne,
    '>=': ge,
    '>' : gt
}

with open('08_input.txt') as f:
    for line in f:
        l = line.split()
        r1, instr, v1, r2, op, v2 = l[0], l[1], int(l[2]), l[4], ops[l[5]], int(l[6])
        program.append((r1, instr, v1, r2, op, v2))

for r1, instr, v1, r2, op, v2 in program:
    if op(registers[r2], v2):
        if instr == 'inc':
            registers[r1] += v1
        elif instr == 'dec':
            registers[r1] -= v1

        if registers[r1] > high:
            high = registers[r1]

print(high)
