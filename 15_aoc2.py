with open('15_input.txt') as f:
    a, b = (int(line.split()[-1]) for line in f)

factor_a = 16807
factor_b = 48271
div = 2147483647
n = 5000000
cmp_a, cmp_b = [], []

def bin_low16(i):
    return '{:016b}'.format(i)[-16:]

while len(cmp_a) < n or len(cmp_b) < n:
    a = a * factor_a % div
    b = b * factor_b % div

    if a % 4 == 0: cmp_a.append(a)
    if b % 8 == 0: cmp_b.append(b)

count = sum(bin_low16(ca) == bin_low16(cb) for ca, cb in zip(cmp_a, cmp_b))
print(count)
