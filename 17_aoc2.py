N = 50000000
steps = 312
pos = 0
x = -1

for i in range(1, N+1):
    pos = (pos + steps) % i + 1
    if pos == 1:
        x = i

print(x)
