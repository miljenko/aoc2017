from collections import defaultdict
from string import ascii_lowercase

program = []
registers = defaultdict(int)

with open('23_input.txt') as f:
    for line in f:
        instr, params = line[:3], [p if p in ascii_lowercase else int(p)
                                   for p in line[3:].strip().split()]
        program.append((instr, params))

def val(x):
    return x if isinstance(x, int) else registers[x]

muls = 0
lineno = 0
while lineno < len(program):
    instr, params = program[lineno]
    # print(instr, params, registers)

    if instr == 'set':
        registers[params[0]] = val(params[1])
    elif instr == 'sub':
        registers[params[0]] -= val(params[1])
    elif instr == 'mul':
        registers[params[0]] *= val(params[1])
        muls += 1
    elif instr == 'jnz':
        if val(params[0]) != 0:
            lineno += val(params[1])
            continue

    lineno += 1

# print(registers)
print(muls)
