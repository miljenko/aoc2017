from functools import reduce
from operator import xor

def knot_hash(s):
    ''' algorithm from solution of day 10 part 2 '''

    def knot_hash_round(l, pos, skip):
        for i in inp:
            if pos+i >= n:
                over = pos + i -n
                rev = (l[pos:] + l[0:over])[::-1]
                l[pos:] = rev[:i-over]
                l[:over] = rev[i-over:]
            else:
                l[pos:pos+i] = reversed(l[pos:pos+i])

            pos += i + skip
            pos %= n
            skip += 1

        return l, pos, skip

    inp = [ord(c) for c in s]
    inp.extend((int(i) for i in '17, 31, 73, 47, 23'.split(',')))
    n = 256
    l = list(range(n))
    pos = 0
    skip = 0

    for _ in range(64):
        l, pos, skip = knot_hash_round(l, pos, skip)

    dense_hash = [reduce(xor, l[k*16:(k+1)*16]) for k in range(n//16)]
    final_hash = ''.join('%02x' % d for d in dense_hash)

    return final_hash
