def walk(pos):
    seen.add(pos)
    for n in tree[pos]:
        if n not in seen:
            walk(n)

tree = {}
seen = set()

with open('12_input.txt') as f:
    for line in f:
        s = line.split(' <-> ')
        i, links = int(s[0]), [int(n) for n in s[1].split(', ')]
        tree[i] = links

walk(0)
print(len(seen))
