def move_scanner(layers):
    for layer in layers:
        d = layer['depth']
        if d <= 1:
            continue

        next_ = layer['scanner'] + layer['dir']
        if next_ < 0 or next_ >= layer['depth']:
            layer['dir'] *= -1

        layer['scanner'] += layer['dir']

    return layers

inp = {}

with open('13_input.txt') as f:
    for line in f:
        s = line.strip().split(': ')
        inp[int(s[0])] = int(s[1])

n = max(inp.keys()) + 1
layers = [{'depth': inp.get(i, 0), 'dir': 1, 'scanner': 0} for i in range(n)]
severity = 0

for ps in range(n):
    current = layers[ps]
    if current['scanner'] == 0:
        severity += ps * current['depth']

    layers = move_scanner(layers)

print(severity)
