with open('10_input.txt') as f:
    inp = [int(i) for i in f.read().split(',')]

n = 256
l = list(range(n))
pos = 0
skip = 0

for i in inp:
    # print(l, f'pos={pos}, len={i}, skip={skip}')
    if pos+i >= n:
        over = pos + i -n
        rev = (l[pos:] + l[0:over])[::-1]
        l[pos:] = rev[:i-over]
        l[:over] = rev[i-over:]
    else:
        l[pos:pos+i] = reversed(l[pos:pos+i])

    pos += i + skip
    pos %= n
    skip += 1

# print(l)
print(l[0]*l[1])
