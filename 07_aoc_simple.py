import re

line_re = re.compile(r'([a-z]+) \((\d+)\)( -> ([a-z]+(, [a-z]+)*))?')
left, right = set(), set()

with open('07_input.txt') as f:
    for line in f:
        g = line_re.match(line).groups()
        left.add(g[0])
        if g[3]:
            right |= set(g[3].split(', '))

print(left - right)
