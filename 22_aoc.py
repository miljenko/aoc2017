grid = {}
with open('22_input.txt') as f:
    lines = f.readlines()
    N = len(lines)
    low = -(N // 2)
    for r, line in enumerate(lines, start=low):
        for c, val in enumerate(line.strip(), start=low):
            grid[r, c] = val

turns = {
    (-1, 0): (0, 1),
    (0, 1): (1, 0),
    (1, 0): (0, -1),
    (0, -1): (-1, 0)
}

pos = (0, 0)
d = (-1, 0)
infections = 0
for i in range(10000):
    if grid.get(pos, '.') == '#':
        grid[pos] = '.'
        d = turns[d] # turn R
    else:
        grid[pos] = '#'
        d = tuple(-i for i in turns[d]) # turn L
        infections += 1

    pos = (pos[0] + d[0], pos[1] + d[1])

print(infections)
