with open('05_input.txt') as f:
    inp = [int(line.strip()) for line in f]

cur_pos, steps = 0, 0

while cur_pos >= 0 and cur_pos < len(inp):
    jmp = inp[cur_pos]
    inp[cur_pos] += 1 if jmp < 3 else -1
    cur_pos += jmp
    steps += 1

print(steps)
